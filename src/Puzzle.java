import java.lang.reflect.Array;
import java.util.*;

public class Puzzle {

    /**
     * Solve the word puzzle.
     *
     * @param args three words (addend1, addend2 and sum)
     */

    public static String firstWord = "kus";
    public static String secondWord = "on";
    public static String sumWord = "koi";
    public static List<List<Integer>> differentSequence = new ArrayList<>();
    public static HashSet<Character> uniqueLetters = new HashSet<>();
    public static List<Integer> numbers = new ArrayList<>();
    public static List<Character> uniqueLettersList = new ArrayList<>();
    public static List<String> words = new ArrayList<>();
    public static List<Integer> wordsInNumbers = new ArrayList<>();
    public static HashMap<Integer, HashMap> success = new HashMap<>();
    public static HashMap<Character, Integer> pairs = new HashMap<>();
    public static int key = 1;

    public static void main(String[] args) {
        correctNumbersWithLetters(firstWord, secondWord, sumWord);
        permute(numbers, 0);
        if (success.isEmpty()) {
            System.out.println("Lahendid puuduvad");
        } else {
            System.out.println(Arrays.asList(success));
        }

        // System.out.println(differentSequence.get(2).toString());

        // throw new RuntimeException ("Nothing implemented yet!"); // delete this
    }

    public static HashMap correctNumbersWithLetters(String firstWord, String secondWord, String sumWord) {
        HashMap optionOfLetters = new HashMap();


        int lenghtOfWord = 19;
        words.add(firstWord);
        words.add(secondWord);
        words.add(sumWord);
        for (int i = 0; i < words.size(); i++) {
            if (words.get(i).toUpperCase().trim().length() < lenghtOfWord) {

                char[] wordsToChar = words.get(i).toCharArray();
                for (int j = 0; j < wordsToChar.length; j++) {
                    // System.out.println(wordsToChar[j]);
                    uniqueLetters.add(wordsToChar[j]);
                    if (!uniqueLettersList.contains(wordsToChar[j])) {
                        uniqueLettersList.add(wordsToChar[j]);
                    }
                }
            } else {
                System.out.println("The word you entered is too long");
            }
        }

       /* Iterator iterator = uniqueLetters.iterator();
        while (iterator.hasNext()) {
            uniqueLettersList.add(iterator.next());
            //System.out.println(iterator.next());
        }*/

        int numberOfUniqueNumbers = uniqueLetters.size();

        for (int i = 0; i < numberOfUniqueNumbers; i++) {
            numbers.add(i);
        }

        HashMap<Integer, Character> combinations = new HashMap<>();
        for (int i = 0; i < numbers.size(); i++) {
            //combinations.put(numbers.get(i), ;
        }


        return optionOfLetters;
    }

    // http://stackoverflow.com/questions/2920315/permutation-of-array
    public static void permute(List<Integer> arr, int k) {

        for (int i = k; i < arr.size(); i++) {
            java.util.Collections.swap(arr, i, k);
            permute(arr, k + 1);
            java.util.Collections.swap(arr, k, i);
        }
        if (k == arr.size() - 1) {

            for (int i = 0; i < arr.size(); i++) {
                pairs.put(uniqueLettersList.get(i), arr.get(i));

            }
            char firstLetter = firstWord.toCharArray()[0];
            char secondWordFirstLetter = secondWord.toCharArray()[0];
            char sumWordFirstLetter = sumWord.toCharArray()[0];
            if (pairs.get(firstLetter) == 0 || pairs.get(secondWordFirstLetter) == 0 || pairs.get(sumWordFirstLetter) == 0) {

                pairs.clear();
                permute(arr, arr.size());
            } else {
                for (int n = 0; n < words.size(); n++) {
                    char[] tempWordToChar = words.get(n).toCharArray();
                    int[] number = new int[tempWordToChar.length];
                    for (int j = 0; j < tempWordToChar.length; j++) {
                        number[j] = pairs.get(tempWordToChar[j]);
                    }

                    StringBuilder stringBuilder = new StringBuilder();
                    for (int num : number) {
                        stringBuilder.append(num);
                    }

                    wordsInNumbers.add(Integer.parseInt(stringBuilder.toString()));

                }
                //System.out.println(Arrays.asList(wordsInNumbers));
                if (wordsInNumbers.get(0) + wordsInNumbers.get(1) == wordsInNumbers.get(2)) {

                    success.put(key, (HashMap) pairs.clone());
                    key++;
                }
                pairs.clear();
                wordsInNumbers.clear();
                //System.out.println(Arrays.asList(wordsInNumbers));


            }
            //System.out.println(Arrays.asList(pairs));

        }

    }
}
